#!/bin/bash
MAP_SERVICE=( 
        "titanoboa:service_auth service_app service_user" 
        "anaconda:service_qr" 
        "atractaspididae:service_order" 
        "boa:service_file service_file_beta" 
        "boiga:service_pm" 
        "content-service.backend:service_content" 
        "copperhead:service_pim" 
        "cottonmouth:service_personalization" 
        "elapidae:service_base" 
        "elapids:service_warehouse" 
        "file-node-extend:service_file_extend" 
        "himehabu:service_dictionary" 
        "languages-service:service_language" 
        "logging-service:service_log" 
        "metadata:service_metadata" 
        "micrelapiinae:service_communication" 
        "notification-service:service_notification" 
        "receipt-service:service_receipt" 
        "rinkhals:service_email" 
        "search-service:service_search"  
        "file-node-extend:service_file_extend" 
    )
SELECTED_SERVICE=$CI_PROJECT_NAME
for service in "${MAP_SERVICE[@]}" ; do
        KEY=${service%%:*}
        VALUE=${service#*:}
        if [ "$CI_PROJECT_NAME" = "$KEY" ];
        then
            SELECTED_SERVICE=$VALUE
        fi
done

case $1 in
  "success" )
    EMBED_COLOR=3066993
    STATUS_MESSAGE="Passed"
    ARTIFACT_URL="$CI_JOB_URL/artifacts/download"
    MESSAGE="✅ Đã deploy ${SELECTED_SERVICE}"
    ;;

  "failure" )
    EMBED_COLOR=15158332
    STATUS_MESSAGE="Failed"
    ARTIFACT_URL="Not available"
    MESSAGE="❌ Đã xảy ra lỗi ${SELECTED_SERVICE}"
    ;;

  * )
    EMBED_COLOR=0
    STATUS_MESSAGE="Status Unknown"
    ARTIFACT_URL="Not available"
    MESSAGE="❌❌❌✅✅✅"
    ;;
esac

shift

if [ $# -lt 1 ]; then
  echo -e "WARNING!!\nYou need to pass the WEBHOOK_URL environment variable as the second argument to this script.\nFor details & guide, visit: https://github.com/DiscordHooks/gitlab-ci-discord-webhook" && exit
fi

AUTHOR_NAME="$(git log -1 "$CI_COMMIT_SHA" --pretty="%aN")"
COMMITTER_NAME="$(git log -1 "$CI_COMMIT_SHA" --pretty="%cN")"
COMMIT_SUBJECT="$(git log -1 "$CI_COMMIT_SHA" --pretty="%s")"
COMMIT_MESSAGE="$(git log -1 "$CI_COMMIT_SHA" --pretty="%b")" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g'

 
if [ -z $CI_MERGE_REQUEST_ID ]; then
  URL=""
else
  URL="$CI_PROJECT_URL/merge_requests/$CI_MERGE_REQUEST_ID"
fi

TIMESTAMP=$(date --utc +%FT%TZ)
echo "$MESSAGE"
if [ -z $LINK_ARTIFACT ] || [ $LINK_ARTIFACT = false ] ; then
  WEBHOOK_DATA='{
    "avatar_url": "https://gitlab.com/favicon.png",
    "embeds": [ {
      "color": '$EMBED_COLOR',
      "author": {
        "name": "'"$MESSAGE"'",
        "url": "'"$CI_PIPELINE_URL"'",
        "icon_url": "https://gitlab.com/favicon.png"
      },
      "title": "'${CI_COMMIT_TITLE}""'",
      "url": "'"$URL"'",
      "description": "'"${CI_COMMIT_DESCRIPTION//$'\n'/ }"\\n\\n""'",
      "fields": [
         {
          "name": "Nhánh",
          "value": "'"[\`$CI_COMMIT_REF_NAME\`]($CI_PROJECT_URL/tree/$CI_COMMIT_REF_NAME)"'",
          "inline": true
        }, 
        {
          "name": "Dự án",
          "value": "'"[\`$CI_PROJECT_NAME\`]($CI_PROJECT_URL)"'",
          "inline": true
        },
        {
          "name": "Máy chủ chạy",
          "value": "'"[\`$CI_RUNNER_DESCRIPTION\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
          "inline": true
        },
        {
          "name": "Tác giả",
          "value": "'"[\`$CI_COMMIT_AUTHOR\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
          "inline": true
        }
        ],
        "timestamp": "'"$TIMESTAMP"'"
      } ]
    }'
else
        WEBHOOK_DATA='{
                "avatar_url": "https://gitlab.com/favicon.png",
                "embeds": [ {
                        "color": '$EMBED_COLOR',
                        "author": {
                          "name": "'"$MESSAGE"'",
                          "url": "'"$CI_PIPELINE_URL"'",
                        "icon_url": "https://gitlab.com/favicon.png"
                        },
                        "title": "'"$COMMIT_SUBJECT"'",
                        "url": "'"$URL"'",
                        "description": "'"${COMMIT_MESSAGE//$'\n'/ }"\\n\\n"$CREDITS"'",
                        "fields": [
                          {
                            "name": "Nhánh",
                            "value": "'"[\`$CI_COMMIT_REF_NAME\`]($CI_PROJECT_URL/tree/$CI_COMMIT_REF_NAME)"'",
                            "inline": true
                          }, 
                          {
                            "name": "Dự án",
                            "value": "'"[\`$CI_PROJECT_NAME\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
                            "inline": true
                          },
                          {
                            "name": "Máy chủ chạy",
                            "value": "'"[\`$CI_RUNNER_DESCRIPTION\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
                            "inline": true
                          },
                          {
                            "name": "Tác giả",
                            "value": "'"[\`$CI_COMMIT_AUTHOR\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
                            "inline": true
                          }
                          {
                            "name": "Artifacts",
                            "value": "'"[\`$CI_JOB_ID\`]($ARTIFACT_URL)"'",
                            "inline": true
                          }
                          ],
                          "timestamp": "'"$TIMESTAMP"'"
                        } ]
                  }'
fi

for ARG in "$@"; do
  echo -e "[Webhook]: Gửi nội dung tới Discord...\\n";

  (curl --fail --progress-bar -A "GitLabCI-Webhook" -H Content-Type:application/json -H X-Author:k3rn31p4nic#8383 -d "$WEBHOOK_DATA" "$ARG" \
  && echo -e "\\n[Webhook]: Đã gửi webhook.") || echo -e "\\n[Webhook]: Không thể gửi webhook."
done